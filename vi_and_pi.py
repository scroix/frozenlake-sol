### MDP Value Iteration and Policy Iteration
### Acknowledgement: start-up codes were adapted with permission from Prof. Emma Brunskill of Stanford University

# Implementation:
# Julien de-Sainte-Croix
# s3369242
# 2019 Semester 1 for Assignment 3 Reinforcement Learning
# COSC1125/1127 Artificial Intelligence

import numpy as np
import gym
import time
from lake_envs import *

np.set_printoptions(precision=3)

"""
For policy_evaluation, policy_improvement, policy_iteration and value_iteration,
the parameters P, nS, nA, gamma are defined as follows:

	P: nested dictionary
		From gym.core.Environment
		For each pair of states in [1, nS] and actions in [1, nA], P[state][action] is a
		tuple of the form (probability, nextstate, reward, terminal) where
			- probability: float
				the probability of transitioning from "state" to "nextstate" with "action"
			- nextstate: int
				denotes the state we transition to (in range [0, nS - 1])
			- reward: int
				either 0 or 1, the reward for transitioning from "state" to
				"nextstate" with "action"
			- terminal: bool
			  True when "nextstate" is a terminal state (hole or goal), False otherwise
	nS: int
		number of states in the environment
	nA: int
		number of actions in the environment
	gamma: float
		Discount factor. Number in range [0, 1)
"""

def policy_evaluation(P, nS, nA, policy, gamma=0.9, tol=1e-3):
	"""Evaluate the value function from a given policy.

	Parameters
	----------
	P, nS, nA, gamma:
		defined at beginning of file
	policy: np.array[nS]
		The policy to evaluate. Maps states to actions.
	tol: float
		Terminate policy evaluation when
			max |value_function(s) - prev_value_function(s)| < tol
	Returns
	-------
	value_function: np.ndarray[nS]
		The value function of the given policy, where value_function[s] is
		the value of state s
	"""

	# Create state value array. i.e [0, 0.20, 0.65, 0... number of states]
	value_function = np.zeros(nS)

	while True:
			delta = 0
			# Loop through the states.
			for state in range(nS):
				value_of_state = 0
				# Loop through the prescribed actions for each state.
				# There are a list of possible outcomes associated with each action.
				for probability, next_state, reward, terminal in P[state][policy[state]]:
					# The value of that state is sumed by the outcome and weighted by the proability.
					value_of_state +=  probability * (reward + gamma * value_function[next_state])
					
				# The difference between the old and new values of the state.
				delta = max(delta, abs(value_function[state] - value_of_state))

				# Update our stored state value with the newly calculated version.
				value_function[state] = value_of_state

			# We'll terminate when the amount of change (delta) to the value of the state is minimal.
			if delta < tol:
				break

	return value_function


def policy_improvement(P, nS, nA, value_from_policy, policy, gamma=0.9):
	"""Given the value function from policy improve the policy.

	Parameters
	----------
	P, nS, nA, gamma:
		defined at beginning of file
	value_from_policy: np.ndarray
		The value calculated from the policy
	policy: np.array
		The previous policy.

	Returns
	-------
	new_policy: np.ndarray[nS]
		An array of integers. Each integer is the optimal action to take
		in that state according to the environment dynamics and the
		given value function.
	"""
	
	new_policy = np.zeros(nS, dtype='int')
	
	# Loop through all the states.
	for s in range(nS):
	  # Loop through each action, attempting to find the action which maximises the value.
		action_values = np.zeros(nA)
		for a in range(nA):
			# Each action outcome has a proability, next state, reward and be a potential terminal state.
			for probability, next_state, reward, terminal in P[s][a]:
				# Determine the value of every action, by summing the value of the outcome weighted against the probability.
				action_values[a] += probability * (reward + gamma * value_from_policy[next_state]) 

		# Compute the best action.
		best_action = np.argmax(action_values)

		# Update the policy with the best action.
		new_policy[s] = best_action

	return new_policy


def policy_iteration(P, nS, nA, gamma=0.9, tol=10e-3):
	"""Runs policy iteration.

	You should call the policy_evaluation() and policy_improvement() methods to
	implement this method.

	Parameters
	----------
	P, nS, nA, gamma:
		defined at beginning of file
	tol: float
		tol parameter used in policy_evaluation()
	Returns:
	----------
	value_function: np.ndarray[nS]
	policy: np.ndarray[nS]
	"""

	value_function = np.zeros(nS)
	policy = np.zeros(nS, dtype=int)

	while True:
		
		# Get the state values.
		value_function = policy_evaluation(P, nS, nA, policy)

		# Get a new policy by selecting the best action per state.
		new_policy = policy_improvement(P, nS, nA, value_function, policy)

		# Get the updated state values based on the new policy.
		new_value_function = policy_evaluation(P, nS, nA, new_policy)

		# Compare the state values of the new and old policies.
		# Stop when iterations no longer produce significantly different results.
		if np.max(abs(value_function - new_value_function)) < tol:
		 break

		# Replace the exisiting policy with our improved version.
		policy = new_policy.copy()

	return value_function, policy

def value_iteration(P, nS, nA, gamma=0.9, tol=1e-3):
	"""
	Learn value function and policy by using value iteration method for a given
	gamma and environment.

	Parameters:
	----------
	P, nS, nA, gamma:
		defined at beginning of file
	tol: float
		Terminate value iteration when
			max |value_function(s) - prev_value_function(s)| < tol
	Returns:
	----------
	value_function: np.ndarray[nS]
	policy: np.ndarray[nS]
	"""

	# Initalise value functions.
	prev_value_function = np.random.rand(nS)
	value_function = np.zeros(nS)

	# Terminate the value iteration when value change is below tolerance.
	while np.average(abs(value_function - prev_value_function)) > tol:

		# Update previous value function before continuing.
		prev_value_function = value_function.copy()
		# Loop through all the states.
		for s in range(nS):
			# Go through all the actions.
			actions = np.zeros(nA)
			for a in range(nA): 
				# Loop through the action outcomes, and compute the value for taking that action.
				for probability, next_state, reward, terminal in P[s][a]:
					actions[a] += probability * (reward + gamma * value_function[next_state])

			# Update value function with the associated value of the best action.
			value_function[s] = np.max(actions)

	# Determine the optimal policy using the value function.
	policy = np.zeros(nS, dtype=int)
	# Loop through all the states.
	for s in range(nS):
		# Then loop through all the actions for that state.
		actions = np.zeros(nA)
		for a in range(nA):
			# Examine all possible outcomes for that action, and compute their values.
			for probability, next_state, reward, terminal in P[s][a]:
				actions[a] += probability * (reward + gamma * value_function[next_state])

		# Update the policy with the best action.
		best_action = np.argmax(actions)
		policy[s] = best_action

	return value_function, policy

def render_single(env, policy, max_steps=100):
  """
    This function does not need to be modified
    Renders policy once on environment. Watch your agent play!

    Parameters
    ----------
    env: gym.core.Environment
      Environment to play on. Must have nS, nA, and P as
      attributes.
    Policy: np.array of shape [env.nS]
      The action to take at a given state
  """

  episode_reward = 0
  ob = env.reset()
  for t in range(max_steps):
    env.render()
    time.sleep(0.25)
    a = policy[ob]
    ob, rew, done, _ = env.step(a)
    episode_reward += rew
    if done:
      break
  env.render()
  if not done:
    print("The agent didn't reach a terminal state in {} steps.".format(max_steps))
  else:
  	print("Episode reward: %f" % episode_reward)

def print_policy(policy, policy_values, action_names):
	''' Thank you @ https://github.com/aaksham/ for the basic print skeleton. '''
	str_policy = policy.astype('str')
	for action_num, action_name in action_names.items():
		np.place(str_policy, policy == action_num, action_name + " : " + str(policy_values[action_num]))

	for i in range(1, str_policy.size + 1):
		print('STATE ' + str(i) + ' : ' + str_policy[i - 1])
		if ((i % 4 == 0)):
			print("-"*9)

	print("-"*25)

# Edit below to run policy and value iteration on different environments and
# visualize the resulting policies in action!
# You may change the parameters in the functions below
if __name__ == "__main__":

	# comment/uncomment these lines to switch between deterministic/stochastic environments
	env = gym.make("Deterministic-4x4-FrozenLake-v0")
	#env = gym.make("Stochastic-4x4-FrozenLake-v0")

	print("\n" + "-"*25 + "\nBeginning Policy Iteration\n" + "-"*25)

	V_pi, p_pi = policy_iteration(env.P, env.nS, env.nA, gamma=0.9, tol=1e-3)
	print_policy(p_pi, V_pi, {0: 'LEFT', 1: 'DOWN', 2: 'RIGHT', 3: 'UP'})
                    
	render_single(env, p_pi, 100)

	print("\n" + "-"*25 + "\nBeginning Value Iteration\n" + "-"*25)

	V_vi, policy_vi = value_iteration(env.P, env.nS, env.nA, gamma=0.9, tol=1e-3)
	print_policy(policy_vi, V_vi, {0: 'LEFT', 1: 'DOWN', 2: 'RIGHT', 3: 'UP'})
	render_single(env, policy_vi, 100)




